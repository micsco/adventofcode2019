import * as fc from './fuelCalculator';
import * as fsWithCallbacks from 'fs';
const fs = fsWithCallbacks.promises;

const main = async () => {
  const strings = (await fs.readFile('../modulemasses.txt'))
    .toString()
    .split('\n');
  const numbers = strings.map(value => +value);

  console.log('Calculating part one');
  console.log(fc.calculateTotal(numbers));
  console.log('Done!');

  console.log('Calculating part two');
  console.log(fc.calculateWeightedTotal(numbers));
  console.log('Done!');
};

main();
