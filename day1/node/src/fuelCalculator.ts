export function calculate(mass: number): number {
  return Math.floor(mass / 3) - 2;
}

export function calculateTotal(masses: number[]): number {
  return masses.reduce((a, b) => a + calculate(b), 0);
}

export function calculateWeighted(mass: number): number {
  const fuel = calculate(mass);
  if (fuel <= 0) return 0;
  else return fuel + calculateWeighted(fuel);
}

export function calculateWeightedTotal(masses: number[]): number {
  return masses.reduce((a, b) => a + calculateWeighted(b), 0);
}
