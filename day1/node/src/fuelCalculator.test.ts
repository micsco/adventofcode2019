import * as fc from './fuelCalculator';

test.each([
  [12, 2],
  [14, 2],
  [1969, 654],
  [100756, 33583],
])('Calculate fuel of mass %i = %i', (mass, expectedFuel) => {
  expect(fc.calculate(mass)).toBe(expectedFuel);
});

type CalculateTotalTestInterface = [number[], number];

test.each<CalculateTotalTestInterface>([[[12, 14, 1969, 100756], 34241]])(
  'Calculate total fuel of mass %i = %i',
  (masses, expectedFuel) => {
    expect(fc.calculateTotal(masses)).toBe(expectedFuel);
  }
);

test.each([
  [12, 2],
  [14, 2],
  [1969, 966],
  [100756, 50346],
])('Calculate weighted fuel of mass %i = %i', (mass, expectedFuel) => {
  expect(fc.calculateWeighted(mass)).toBe(expectedFuel);
});

test.each<CalculateTotalTestInterface>([[[12, 14, 1969, 100756], 51316]])(
  'Calculate weighted  total fuel of mass %i = %i',
  (masses, expectedFuel) => {
    expect(fc.calculateWeightedTotal(masses)).toBe(expectedFuel);
  }
);
