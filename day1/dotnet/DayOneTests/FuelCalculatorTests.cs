using System;
using FluentAssertions;
using Xunit;
using DayOne;
using System.Collections.Generic;

namespace DayOneTests
{
    public class FuelCalculatorTests
    {
        [Theory]
        [InlineData(12, 2)]
        [InlineData(14, 2)]
        [InlineData(1969, 654)]
        [InlineData(100756, 33583)]
        public void TestCalculator(int mass, int expectedFuel)
        {
            FuelCalculator.Calculate(mass).Should().Be(expectedFuel);
        }

        [Theory]
        [MemberData(nameof(Data))]
        public void TestTotal(List<int> masses, int expectedFuel)
        {
            FuelCalculator.CalculateTotal(masses).Should().Be(expectedFuel);
        }

        public static IEnumerable<object[]> Data =>
            new List<object[]>
            {
                new object[] { (new List<int>() { 12,14,1969,100756}), 34241 },
            };

        [Theory]
        [InlineData(12, 2)]
        [InlineData(14, 2)]
        [InlineData(1969, 966)]
        [InlineData(100756, 50346)]
        public void TestWeightedCalculator(int mass, int expectedFuel) {
            FuelCalculator.CalculateWeighted(mass).Should().Be(expectedFuel);
        }

        [Theory]
        [MemberData(nameof(WeightedData))]
        public void TestWeightedTotal(List<int> masses, int expectedFuel)
        {
            FuelCalculator.CalculateWeightedTotal(masses).Should().Be(expectedFuel);
        }

        public static IEnumerable<object[]> WeightedData =>
            new List<object[]>
            {
                new object[] { (new List<int>() { 12,14,1969,100756}), 51316 },
            };

    }
}
