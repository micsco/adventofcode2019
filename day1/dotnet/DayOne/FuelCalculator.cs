using System;
using System.Linq;
using System.Collections.Generic;

namespace DayOne
{
    public class FuelCalculator
    {
        public static int Calculate(int mass)
        {
            return ((int)Math.Floor(mass * 1.0 / 3) - 2);
        }

        public static int CalculateTotal(List<int> masses) {
            return masses.Sum(mass => Calculate(mass));
        }

        public static int CalculateWeighted(int mass) {
            var fuel = Calculate(mass);
            if(fuel <= 0) return 0;
            else return (fuel + CalculateWeighted(fuel));
        }

        public static int CalculateWeightedTotal(List<int> masses) {
            return masses.Sum(mass => CalculateWeighted(mass));
        }
    }
}