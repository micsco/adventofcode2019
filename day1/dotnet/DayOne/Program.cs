﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DayOne
{
    class Program
    {
        private static readonly string LOG_PATH = "../modulemasses.txt";

        static void Main(string[] args)
        {
            var inputFile = File.ReadAllLines(LOG_PATH);
            var stringList = new List<string>(inputFile);
            var intList = stringList.Select(int.Parse).ToList();

            Console.WriteLine("Calculating Part One");
            Console.WriteLine(FuelCalculator.CalculateTotal(intList));
            Console.WriteLine("Done!");

            Console.WriteLine("Calculating Part Two");
            Console.WriteLine(FuelCalculator.CalculateWeightedTotal(intList));
            Console.WriteLine("Done!");
        }
    }
}
